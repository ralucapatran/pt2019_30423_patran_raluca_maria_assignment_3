package presentation;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class ProductOperationsView extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JButton addProductButton;
	private JButton editProductButton;
	private JButton deleteProductButton;
	
	private JTextField idText;
	private JTextField nameText;
	private JTextField priceText;
	private JTextField quantityText;

	
	public ProductOperationsView() {

		Font font1 = new Font("SansSerif", Font.BOLD, 30);
		
		this.setBounds(1000, 1000, 1500, 1000);
		getContentPane().setLayout(null);
		
		JLabel id = new JLabel("Product Id");
		id.setBounds(450, 870, 200, 100);
		id.setFont(font1);
		getContentPane().add(id);

		idText = new JTextField();
		idText.setBounds(450, 950, 200, 40);
		idText.setFont(font1);
		getContentPane().add(idText);
		
		JLabel name = new JLabel("Name");
		name.setBounds(450, 970, 200, 100);
		name.setFont(font1);
		getContentPane().add(name);

		nameText = new JTextField();
		nameText.setBounds(450, 1050, 200, 40);
		nameText.setFont(font1);
		getContentPane().add(nameText);
		
		JLabel price = new JLabel("Price");
		price.setBounds(450, 1070, 200, 100);
		price.setFont(font1);
		getContentPane().add(price);

		priceText = new JTextField();
		priceText.setBounds(450, 1150, 200, 40);
		priceText.setFont(font1);
		getContentPane().add(priceText);
		
		JLabel quantity = new JLabel("Quantity");
		quantity.setBounds(450, 1170, 300, 100);
		quantity.setFont(font1);
		getContentPane().add(quantity);

		
		quantityText = new JTextField();
		quantityText.setBounds(450, 1240, 200, 40);
		quantityText.setFont(font1);
		getContentPane().add(quantityText);
		
		addProductButton = new JButton("Add Product");
		addProductButton.setBounds(450,1300, 400, 100);
		getContentPane().add(addProductButton);
		addProductButton.setFont(font1);
		
		editProductButton = new JButton("Show Table");
		editProductButton.setBounds(1050, 1300, 400, 100);
		getContentPane().add(editProductButton);
		editProductButton.setFont(font1);
		
		deleteProductButton = new JButton("Remove Product");
		deleteProductButton.setBounds(1650, 1300, 400, 100);
		getContentPane().add(deleteProductButton);
		deleteProductButton.setFont(font1);
	}
	
	public static void infoBox(String infoMessage, String titleBar)
    {
        JOptionPane.showMessageDialog(null, infoMessage, "InfoBox: " + titleBar, JOptionPane.INFORMATION_MESSAGE);
    }

	public void addProductButtonActionListener(final ActionListener actionListener) {
		addProductButton.addActionListener(actionListener);
	}

	public void editProductButtonActionListener(final ActionListener actionListener) {
		editProductButton.addActionListener(actionListener);
	}

	public void deleteProductButtonActionListener(final ActionListener actionListener) {
		deleteProductButton.addActionListener(actionListener);
	}

	public String getId() {
		return idText.getText();
	}

	public String getName() {
		return nameText.getText();
	}

	public String getPrice() {
		return priceText.getText();
	}

	public String getQuantity() {
		return quantityText.getText();
	}

}
