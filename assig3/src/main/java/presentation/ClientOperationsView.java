package presentation;

import java.awt.Font;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class ClientOperationsView extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JButton addClientButton;
	private JButton editClientButton;
	private JButton deleteClientButton;

	private JTextField idText;
	private JTextField nameText;
	private JTextField addressText;
	private JTextField phoneText;

	public ClientOperationsView() {

		Font font1 = new Font("SansSerif", Font.BOLD, 30);

		this.setBounds(1000, 1000, 1500, 1000);
		getContentPane().setLayout(null);
	
	
		JLabel id = new JLabel("clientId");
		id.setBounds(450, 870, 200, 100);
		id.setFont(font1);
		getContentPane().add(id);

		idText = new JTextField();
		idText.setBounds(450, 950, 200, 40);
		idText.setFont(font1);
		getContentPane().add(idText);

		JLabel name = new JLabel("Name");
		name.setBounds(450, 970, 200, 100);
		name.setFont(font1);
		getContentPane().add(name);

		nameText = new JTextField();
		nameText.setBounds(450, 1050, 200, 40);
		nameText.setFont(font1);
		getContentPane().add(nameText);

		JLabel address = new JLabel("Address");
		address.setBounds(450, 1070, 200, 100);
		address.setFont(font1);
		getContentPane().add(address);

		addressText = new JTextField();
		addressText.setBounds(450, 1150, 200, 40);
		addressText.setFont(font1);
		getContentPane().add(addressText);

		JLabel phone = new JLabel("Phone Number");
		phone.setBounds(450, 1170, 300, 100);
		phone.setFont(font1);
		getContentPane().add(phone);

		phoneText = new JTextField();
		phoneText.setBounds(450, 1240, 200, 40);
		phoneText.setFont(font1);
		getContentPane().add(phoneText);

		addClientButton = new JButton("Add Client");
		addClientButton.setBounds(450, 1300, 400, 100);
		getContentPane().add(addClientButton);
		addClientButton.setFont(font1);

		editClientButton = new JButton("Show Table");
		editClientButton.setBounds(1050, 1300, 400, 100);
		getContentPane().add(editClientButton);
		editClientButton.setFont(font1);

		deleteClientButton = new JButton("Remove Client");
		deleteClientButton.setBounds(1650, 1300, 400, 100);
		getContentPane().add(deleteClientButton);
		deleteClientButton.setFont(font1);
		
	}
	
	public static void infoBox(String infoMessage, String titleBar)
    {
        JOptionPane.showMessageDialog(null, infoMessage, "InfoBox: " + titleBar, JOptionPane.INFORMATION_MESSAGE);
    }

	public void addClientButtonActionListener(final ActionListener actionListener) {
		addClientButton.addActionListener(actionListener);
	}

	public void editClientButtonActionListener(final ActionListener actionListener) {
		editClientButton.addActionListener(actionListener);
	}

	public void deleteClientButtonActionListener(final ActionListener actionListener) {
		deleteClientButton.addActionListener(actionListener);
	}

	public String getId() {
		return idText.getText();
	}

	public String getName() {
		return nameText.getText();
	}

	public String getAddress() {
		return addressText.getText();
	}

	public String getPhone() {
		return phoneText.getText();
	}
	

}
	
