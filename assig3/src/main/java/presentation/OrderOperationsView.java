package presentation;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class OrderOperationsView extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton placeOrderButton ;
	private JButton showTableButton;
	
	private JTextField orderIdText;
	private JTextField clientIdText;
	private JTextField productIdText;
	private JTextField quantityText;

	
	public OrderOperationsView() {

		Font font1 = new Font("SansSerif", Font.BOLD, 30);
		
		this.setBounds(1000, 1000, 1500, 1000);
		getContentPane().setLayout(null);
		
		JLabel id = new JLabel("Order Id");
		id.setBounds(450, 870, 200, 100);
		id.setFont(font1);
		getContentPane().add(id);

		orderIdText = new JTextField();
		orderIdText.setBounds(450, 950, 200, 40);
		orderIdText.setFont(font1);
		getContentPane().add(orderIdText);
		
		JLabel name = new JLabel("Product Id");
		name.setBounds(450, 970, 200, 100);
		name.setFont(font1);
		getContentPane().add(name);

		productIdText = new JTextField();
		productIdText.setBounds(450, 1050, 200, 40);
		productIdText.setFont(font1);
		getContentPane().add(productIdText);
		
		JLabel price = new JLabel("ClientId");
		price.setBounds(450, 1070, 200, 100);
		price.setFont(font1);
		getContentPane().add(price);

		clientIdText = new JTextField();
		clientIdText.setBounds(450, 1150, 200, 40);
		clientIdText.setFont(font1);
		getContentPane().add(clientIdText);
		
		JLabel quantity = new JLabel("Quantity");
		quantity.setBounds(450, 1170, 300, 100);
		quantity.setFont(font1);
		getContentPane().add(quantity);

		
		quantityText = new JTextField();
		quantityText.setBounds(450, 1240, 200, 40);
		quantityText.setFont(font1);
		getContentPane().add(quantityText);
		
		showTableButton = new JButton("Show Table");
		showTableButton.setBounds(1050, 1300, 400, 100);
		getContentPane().add(showTableButton);
		showTableButton.setFont(font1);
		
		placeOrderButton = new JButton("Place order");
		placeOrderButton .setBounds(450,1300, 400, 100);
		getContentPane().add(placeOrderButton );
		placeOrderButton .setFont(font1);
		
	}
	
	public static void infoBox(String infoMessage, String titleBar)
    {
        JOptionPane.showMessageDialog(null, infoMessage, "InfoBox: " + titleBar, JOptionPane.INFORMATION_MESSAGE);
    }

	public void placeOrderButtonActionListener(final ActionListener actionListener) {
		placeOrderButton.addActionListener(actionListener);
	}

	public void showTableButtonActionListener(final ActionListener actionListener) {
		showTableButton.addActionListener(actionListener);
	}

	public String getId() {
		return orderIdText.getText();
	}

	public String getName() {
		return productIdText.getText();
	}

	public String getPrice() {
		return clientIdText.getText();
	}

	public String getQuantity() {
		return quantityText.getText();
	}

}

