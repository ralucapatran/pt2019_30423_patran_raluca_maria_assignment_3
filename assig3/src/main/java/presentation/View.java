package presentation;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class View extends JFrame {

	private static final long serialVersionUID = 1L;


	private JButton clientOp;
	private JButton productOp;
	private JButton placeOrder;

	public View() {

		Font font1 = new Font("SansSerif", Font.BOLD, 30);
		
		this.setBounds(1000, 1000, 1500, 1500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		clientOp = new JButton("Client Operations");
		clientOp.setBounds(550,200, 400, 200);
		getContentPane().add(clientOp);
		clientOp.setFont(font1);
		
		productOp = new JButton("Product Operations");
		productOp.setBounds(550, 500, 400, 200);
		getContentPane().add(productOp);
		productOp.setFont(font1);
		
		placeOrder = new JButton("Place Order");
		placeOrder.setBounds(550, 800, 400, 200);
		getContentPane().add(placeOrder);
		placeOrder.setFont(font1);
		
		
	}
	
	public void clientOpButtonActionListener(final ActionListener actionListener) {
		clientOp.addActionListener(actionListener);
	}
	
	public void productOpButtonActionListener(final ActionListener actionListener) {
		productOp.addActionListener(actionListener);
	}
	
	public void placeOrderButtonActionListener(final ActionListener actionListener) {
		placeOrder.addActionListener(actionListener);
	}
}

		