package model;

public class Client {

	private int clientId;
	private String name;
	private String address;
	private String phoneNumber;

	public Client(int clientId,String name,String address,String phoneNumber) {
		this.clientId=clientId;
		this.name=name;
		this.address=address;
		this.phoneNumber=phoneNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getId() {
		return clientId;
	}

	public void setId(int id) {
		this.clientId = id;
	}

	public String getPhone() {
		return phoneNumber;
	}

	public void setPhone(String phone) {
		this.phoneNumber = phone;
	}

}
