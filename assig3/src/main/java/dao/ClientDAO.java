package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dataAccessLayer.Database;
import model.Client;

public class ClientDAO {

	private static  Database database;

	public ClientDAO() {
		try {
			database = Database.getInstance();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Client> findAll() {
		ResultSet resultSet = database.executeStatement("select * from client;");

		final List<Client> clients = new ArrayList<>();

		if (resultSet != null) {
			try {
				while (resultSet.next()) {
					final String id = resultSet.getString("clientId");
					final String name = resultSet.getString("name");
					final String address = resultSet.getString("address");
					final String phone = resultSet.getString("phoneNumber");
					final Client client = new Client(Integer.parseInt(id),name,address,phone);
					client.setId(Integer.parseInt(id));
					client.setName(name);
					client.setAddress(address);
					client.setPhone(phone);
					
					clients.add(client);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return clients;
	}

	public void save(final Client client) {
		final Connection connection = database.getConnection();

		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement(
					"insert into client (clientId, name, address, phoneNumber) values (?, ?, ?, ?)");
			preparedStatement.setString(1, Integer.toString(client.getId()));
			preparedStatement.setString(2, client.getName());
			preparedStatement.setString(3, client.getAddress());
			preparedStatement.setString(4, client.getPhone());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static ArrayList<Client> select(){
		Connection dbConnection = database.getConnection();
		PreparedStatement selectStatement = null;
		ResultSet rs = null;
		ArrayList<Client> customers= new ArrayList<Client>();
		
		try{
			selectStatement = dbConnection.prepareStatement("SELECT * FROM client");
			rs = selectStatement.executeQuery();
			
			while(rs.next()){
				int idCustomer= rs.getInt("clientId");
				String name= rs.getString("name");
				String address= rs.getString("address");
				String phone= rs.getString("phoneNumber");
				
				Client c= new Client(idCustomer,name,address,phone);
				customers.add(c);
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return customers;
	}

}
