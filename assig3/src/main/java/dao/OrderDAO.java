package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dataAccessLayer.Database;
import model.Order;

public class OrderDAO {
	private static  Database database;

	public OrderDAO() {
		try {
			database = Database.getInstance();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Order> findAll() {
		ResultSet resultSet = database.executeStatement("select * from business.order;");

		final List<Order> orders = new ArrayList<>();

		if (resultSet != null) {
			try {
				while (resultSet.next()) {
					final String orderId = resultSet.getString("orderId");
					final String productId = resultSet.getString("productId");
					final String clientId = resultSet.getString("clientId");
					final String quantity = resultSet.getString("quantity");
					final Order order = new Order(Integer.parseInt(orderId),Integer.parseInt(productId),Integer.parseInt(clientId),Integer.parseInt(quantity));
					order.setOrderId(Integer.parseInt(orderId));
					order.setProductId(Integer.parseInt(productId));
					order.setClientId(Integer.parseInt(clientId));
					order.setQuantity(Integer.parseInt(quantity));
					
					orders.add(order);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return orders;
	}

	public void save(final Order order) {
		final Connection connection = database.getConnection();

		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement(
					"insert into business.order (orderId, productId, clientId, quantity) values (?, ?, ?, ?)");
			preparedStatement.setString(1, Integer.toString(order.getOrderId()));
			preparedStatement.setString(2, Integer.toString(order.getProductId()));
			preparedStatement.setString(3, Integer.toString(order.getClientId()));
			preparedStatement.setString(4, Integer.toString(order.getQuantity()));
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static ArrayList<Order> select(){
		Connection dbConnection = database.getConnection();
		PreparedStatement selectStatement = null;
		ResultSet rs = null;
		ArrayList<Order> orders= new ArrayList<Order>();
		
		try{
			selectStatement = dbConnection.prepareStatement("SELECT * FROM business.order");
			rs = selectStatement.executeQuery();
			
			while(rs.next()){
				int orderId= rs.getInt("orderId");
				int productId= rs.getInt("productId");
				int clientId= rs.getInt("clientId");
				int quantity= rs.getInt("quantity");
				
				Order c= new Order(orderId,productId,clientId,quantity);
				orders.add(c);
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return orders;
	}


}
