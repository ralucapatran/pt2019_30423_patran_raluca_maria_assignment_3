package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dataAccessLayer.Database;
import model.Product;

public class ProductDAO {

	private static  Database database;

	public ProductDAO() {
		try {
			database = Database.getInstance();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Product> findAll() {
		ResultSet resultSet = database.executeStatement("select * from product;");

		final List<Product> products = new ArrayList<>();

		if (resultSet != null) {
			try {
				while (resultSet.next()) {
					final String productId = resultSet.getString("productId");
					final String productName = resultSet.getString("productName");
					final String productPrice = resultSet.getString("productPrice");
					final String quantity = resultSet.getString("quantity");
					final Product product = new Product(Integer.parseInt(productId),productName,Integer.parseInt(productPrice),Integer.parseInt(quantity));
					product.setProductId(Integer.parseInt(productId));
					product.setProductName(productName);
					product.setProductPrice(Integer.parseInt(productPrice));
					product.setQuantity(Integer.parseInt(quantity));
					
					products.add(product);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return products;
	}

	public void save(final Product product) {
		final Connection connection = database.getConnection();

		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement(
					"insert into product (productId, productName, productPrice, quantity) values (?, ?, ?, ?)");
			preparedStatement.setString(1, Integer.toString(product.getProductId()));
			preparedStatement.setString(2, product.getProductName());
			preparedStatement.setString(3, Integer.toString(product.getProductPrice()));
			preparedStatement.setString(4, Integer.toString(product.getQuantity()));
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static ArrayList<Product> select(){
		Connection dbConnection = database.getConnection();
		PreparedStatement selectStatement = null;
		ResultSet rs = null;
		ArrayList<Product> products= new ArrayList<Product>();
		
		try{
			selectStatement = dbConnection.prepareStatement("SELECT * FROM product");
			rs = selectStatement.executeQuery();
			
			while(rs.next()){
				int idProduct= rs.getInt("productId");
				String productName= rs.getString("productName");
				int price= rs.getInt("productPrice");
				int quantity= rs.getInt("quantity");
				
				Product c= new Product(idProduct,productName,price,quantity);
				products.add(c);
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return products;
	}

}
