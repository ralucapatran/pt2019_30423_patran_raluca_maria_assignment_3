package businessLayer;

import javax.swing.JFrame;
import javax.swing.UIManager;

import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import model.Client;
import model.Order;
import model.Product;
import operations.RemoveClient;
import operations.RemoveProduct;
import presentation.ClientOperationsView;
import presentation.OrderOperationsView;
import presentation.ProductOperationsView;
import presentation.View;
import tables.ClientTable;
import tables.OrderTable;
import tables.ProductTable;

public class App extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private View view;
	private ClientOperationsView clientOpView;
	private ProductOperationsView productOpView;
	private OrderOperationsView placeOrderView;


	public void start() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		view = new View();
		clientOpView = new ClientOperationsView();
		productOpView = new ProductOperationsView();
		placeOrderView = new OrderOperationsView();
		initializeButtonListenersForMainView();

		clientOpView.setLocationRelativeTo(null);

		view.setVisible(true);
		view.setLocationRelativeTo(null);

	}

	

	private void initializeButtonListenersForMainView() {
		view.clientOpButtonActionListener(e -> {

			clientOpView.setVisible(true);
			clientOpView.setLocationRelativeTo(null);

		});

		view.productOpButtonActionListener(e -> {

			productOpView.setVisible(true);
			productOpView.setLocationRelativeTo(null);

		});
		
		view.placeOrderButtonActionListener(e -> {

			placeOrderView.setVisible(true);
			placeOrderView.setLocationRelativeTo(null);

		});



		clientOpView.addClientButtonActionListener(e -> {

			ClientDAO userService = new ClientDAO();

			
			Client client = new Client(Integer.parseInt(clientOpView.getId()),clientOpView.getName(),clientOpView.getAddress(),clientOpView.getPhone());
			client.setId(Integer.parseInt(clientOpView.getId()));
			client.setName(clientOpView.getName());
			client.setAddress(clientOpView.getAddress());
			client.setPhone(clientOpView.getPhone());
			
			ClientOperationsView.infoBox("Client added successfully!", "Operation");

			userService.save(client);
			//System.out.println(table.getRowCount());

		});

		clientOpView.deleteClientButtonActionListener(e -> {
			RemoveClient removeClient = new RemoveClient();
			removeClient.save(Integer.parseInt(clientOpView.getId()));
			ClientOperationsView.infoBox("Client removed successfully!", "Operation");

		});
		
		clientOpView.editClientButtonActionListener(e -> {
            ClientTable tabel= new ClientTable();
			
			tabel.setVisible(true);

		});
		
		productOpView.addProductButtonActionListener(e -> {

			ProductDAO userService = new ProductDAO();

			
			Product product = new Product(Integer.parseInt(productOpView.getId()),productOpView.getName(),Integer.parseInt(productOpView.getPrice()),Integer.parseInt(productOpView.getQuantity()));
			product.setProductId(Integer.parseInt(productOpView.getId()));
			product.setProductName(productOpView.getName());
			product.setProductPrice(Integer.parseInt(productOpView.getPrice()));
			product.setQuantity(Integer.parseInt(productOpView.getQuantity()));
			
			ClientOperationsView.infoBox("Product added successfully!", "Operation");

			userService.save(product);
			//System.out.println(table.getRowCount());

		});
		
		productOpView.deleteProductButtonActionListener(e -> {
			RemoveProduct removeProduct = new RemoveProduct();
			removeProduct.save(Integer.parseInt(productOpView.getId()));
			ClientOperationsView.infoBox("Product removed successfully!", "Operation");

		});
		
		productOpView.editProductButtonActionListener(e -> {
            ProductTable tabel= new ProductTable();
			
			tabel.setVisible(true);

		});
		
		placeOrderView.placeOrderButtonActionListener(e -> {

			OrderDAO userService = new OrderDAO();

			
			Order order = new Order(Integer.parseInt(placeOrderView.getId()),Integer.parseInt(placeOrderView.getName()),Integer.parseInt(placeOrderView.getPrice()),Integer.parseInt(placeOrderView.getQuantity()));
			order.setOrderId(Integer.parseInt(placeOrderView.getId()));
			order.setProductId(Integer.parseInt(placeOrderView.getName()));
			order.setClientId(Integer.parseInt(placeOrderView.getPrice()));
			order.setQuantity(Integer.parseInt(placeOrderView.getQuantity()));
			
			OrderOperationsView.infoBox("Order placed successfully!", "Operation");

			userService.save(order);
			//System.out.println(table.getRowCount());

		});
		
		placeOrderView.showTableButtonActionListener(e -> {
            OrderTable tabel= new OrderTable();
			
			tabel.setVisible(true);

		});
	}

}
