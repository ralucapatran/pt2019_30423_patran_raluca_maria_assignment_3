package bll;

import java.util.ArrayList;

import dao.OrderDAO;
import model.Order;

public class OrderBLL {
	public ArrayList<Order> selectAllOrders(){
		return OrderDAO.select();
	}
}
