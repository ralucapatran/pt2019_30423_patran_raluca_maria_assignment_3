package bll;

import java.util.ArrayList;

import dao.ClientDAO;
import model.Client;

public class ClientBLL {
	
	public ArrayList<Client> selectAllCustomers(){
		return ClientDAO.select();
	}

}
