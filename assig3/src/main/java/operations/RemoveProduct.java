package operations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import dataAccessLayer.Database;

public class RemoveProduct {
	
	private Database database;

	public RemoveProduct() {
			try {
				database = Database.getInstance();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	public void executeRemove() {
		database.executeStatement("delete from business.product");
	}
	
	public void save(int id) {
		final Connection connection = database.getConnection();

		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement(
					"delete from business.product where productId = ? ");
			preparedStatement.setString(1, Integer.toString(id));
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
