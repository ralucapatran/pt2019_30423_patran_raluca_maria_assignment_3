package operations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import dataAccessLayer.Database;

public class RemoveClient {

	private Database database;

	public RemoveClient() {
			try {
				database = Database.getInstance();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	public void executeRemove() {
		database.executeStatement("delete from business.client");
	}
	
	public void save(int id) {
		final Connection connection = database.getConnection();

		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement(
					"delete from business.client where clientId = ? ");
			preparedStatement.setString(1, Integer.toString(id));
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
