package tables;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import bll.ProductBLL;
import dao.AbstractDAO;

public class ProductTable extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ProductTable(){
		setTitle("Products Table");
		setResizable(true);
		this.setBounds(400, 100,1000,1000);
		init();
	}
	
	public void init(){
		JPanel panel= new JPanel();
		
				ProductBLL c= new ProductBLL();
				ArrayList<Object> list= new ArrayList<Object>();
				list.addAll(c.selectAllProducts());
				
				JTable table=AbstractDAO.createTable(list);
				
				//System.out.println(table.getRowCount());
				
				JScrollPane scroll = new JScrollPane(table);
				panel.add(table.getTableHeader());
				panel.add(scroll);
		
		setContentPane(panel);
		setVisible(true);
	}

	

}
