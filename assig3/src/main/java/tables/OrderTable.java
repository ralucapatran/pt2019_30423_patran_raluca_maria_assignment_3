package tables;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import bll.OrderBLL;
import dao.AbstractDAO;

public class OrderTable extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OrderTable(){
		setTitle("Orders Table");
		setResizable(true);
		this.setBounds(400, 100,1000,1000);
		init();
	}
	
	public void init(){
		JPanel panel= new JPanel();
		
				OrderBLL c= new OrderBLL();
				ArrayList<Object> list= new ArrayList<Object>();
				list.addAll(c.selectAllOrders());
				
				JTable table=AbstractDAO.createTable(list);
				
				//System.out.println(table.getRowCount());
				
				JScrollPane scroll = new JScrollPane(table);
				panel.add(table.getTableHeader());
				panel.add(scroll);
		
		setContentPane(panel);
		setVisible(true);
	}

}
